package strings;

import java.util.HashSet;
import java.util.Set;

public class DuplicateChars {

	public static void main(String args[]) {
		printDuplicateChars("aabbfghrr");
	}
	
	public static void printDuplicateChars(String input) {
		char [] inpArray = input.toCharArray();
		
		Set<Character> mySet = new HashSet<>();
		for(char c : inpArray) {
			if(!mySet.add(c)) {
				System.out.println(c);
			}
		}
		
	}
}
